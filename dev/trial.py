import numpy as np

from math import ceil


if __name__ == '__main__':
    r = np.random.random()

    print('Random: ', r)

    min_val = 0
    max_val = 6

    resampled = r * (max_val - min_val) + min_val

    print('Resampled: ', resampled)

    print('ceil: ', ceil(resampled))
