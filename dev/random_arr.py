import numpy as np


if __name__ == '__main__':
    r = np.random.rand(1000, 5)

    size = np.sqrt(sum(r.T**2).T)

    unit_vectors = r/size[:, None]

    print(unit_vectors)

    print(np.sqrt(sum(unit_vectors.T**2)))
