#!/usr/bin/env python3

import numpy as np


# ========= Config =========

config = {
    'n_trials': 100000
}

# ==========================


if __name__ == '__main__':
    n_trials = config['n_trials']

    x = np.random.random(n_trials)
    y = np.random.random(n_trials)

    inside_circle = np.sqrt(x**2 + y**2) <= 1

    pi = 4 * sum(inside_circle) / len(inside_circle)

    stdev = np.sqrt(np.abs(pi**2 - np.pi**2) / float(n_trials))

    print('***** Results *****')
    print('Pi value found: ', pi, ' +/- %.2e' % stdev)
    print('*******************')
