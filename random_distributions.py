import time

import numpy as np
import cupy as cp
import matplotlib.pyplot as pl
import matplotlib.patches as mpatches

from scipy.optimize import curve_fit
from scipy import asarray as ar, exp

from math import ceil


config = {
    'distributions': {
        'distribution_type': 'Normal',
        'n_distributions': 100,
        'n_events': 1000000000,
        'min_val': 4,
        'max_val': 12
    },
    'output': {
        'filename': 'central_limit.png'
    }
}


def gaus(x, a, x0, sigma):
    return a*exp(-(x-x0)**2/(2*sigma**2))


def get_chi2(data_values, fit_values):
    """
    Returns the value of chi2 / NDOF
    """
    return sum((data_values - fit_values) ** 2 / fit_values) / len(fit_values)


if __name__ == '__main__':
    prior = time.time()

    n_events = config['distributions']['n_events']
    n_distributions = config['distributions']['n_distributions']

    max_n_events_per_filling = 150000000

    n_fillings = ceil(n_events / max_n_events_per_filling)

    min_val = config['distributions']['min_val']
    max_val = config['distributions']['max_val']

    lambdas = cp.random.randint(
        min_val, max_val, n_distributions)

    res = None
    bins = None

    for i in range(n_fillings):
        if n_events > max_n_events_per_filling:
            current_n_events = max_n_events_per_filling
        else:
            current_n_events = n_events

        generated_events = cp.zeros(current_n_events)

        for l in lambdas:
            if config['distributions']['distribution_type'].lower() == 'poisson':
                generated_events += cp.random.poisson(l, size=current_n_events)

            elif config['distributions']['distribution_type'].lower() == 'normal':
                generated_events += cp.random.normal(l, size=current_n_events)

            elif config['distributions']['distribution_type'].lower() == 'exponential':
                generated_events += cp.random.exponential(
                    l, size=current_n_events)

            else:
                raise NotImplementedError(
                    config['distributions']['distribution_type'] + ' not defined')

        if bins is None:
            hist, bins = cp.histogram(generated_events)
            res = hist
        else:
            hist, bins = cp.histogram(generated_events, bins=bins)
            res += hist

        n_events -= current_n_events

    print('Generated ', config['distributions']['n_events'], 'random numbers from',
          config['distributions']['n_distributions'], 'distributions in %f.2 s' % (time.time() - prior))

    bin_centres = (bins[:-1] + bins[1:]) / 2

    bin_centres = cp.asnumpy(bin_centres)
    res = cp.asnumpy(res)

    popt, pcov = curve_fit(gaus, bin_centres, res, p0=[
                           max(res), 
                           (max(bin_centres) + min(bin_centres)) / 2, 
                           (max(bin_centres) - min(bin_centres)) / 2])

    gaus_fit = gaus(bin_centres, *popt)

    chi2 = get_chi2(res, gaus_fit)

    print('Chi2: %.3f' % chi2)

    x_axis_fit = np.linspace(min(bin_centres), max(bin_centres), 100)

    gaus_fit_for_plotting = gaus(x_axis_fit, *popt)

    patches = [mpatches.Patch(color='b', label='Random numbers'),
               mpatches.Patch(color='tab:red', label='Gaussian fit')]

    fig = pl.figure(figsize=[9, 6])
    ax = pl.subplot()

    ax.text(0.5, 0.75, 'chi2/DOF: %.2f' % chi2,
            horizontalalignment='left',
            verticalalignment='top',
            weight='medium',
            fontsize=20,
            transform=ax.transAxes)

    pl.plot(bin_centres, res, 'bo')
    pl.plot(x_axis_fit, gaus_fit_for_plotting, color='tab:red')
    pl.legend(handles=patches, fontsize=16, loc=1)
    pl.savefig(config['output']['filename'])
