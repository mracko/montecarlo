#!/usr/bin/env python3

import numpy as np


# ========= Config =========

config = {
    'lower_bound': 0,
    'upper_bound': 5,
    'n_samples': 100000
}

# === integrand function ===


def f(x: np.ndarray):
    return 1 / (1 + x**2)

# ==========================


class IntegralCalculator(object):
    def __init__(self,
                 n_samples: int,
                 integrand: callable):
        self.integrand = integrand

        self.n_samples = n_samples

        self.random_sample = None

        self.lower_bound = None
        self.upper_bound = None

    def set_integrand(self,
                      integrand: callable):
        self.integrand = integrand

    def _random_sample(self,
                       lower_bound: float,
                       upper_bound: float):
        self.random_sample = np.random.random(self.n_samples)

        self.random_sample *= (upper_bound - lower_bound)
        self.random_sample += lower_bound

    def evaluate(self,
                 lower_bound: float,
                 upper_bound: float):
        self._random_sample(lower_bound, upper_bound)

        res = sum(self.integrand(self.random_sample))

        res *= (upper_bound - lower_bound)
        res /= self.n_samples

        return res  # TODO: get error


if __name__ == '__main__':
    calculator = IntegralCalculator(n_samples=config['n_samples'], integrand=f)

    res = calculator.evaluate(
        lower_bound=config['lower_bound'], upper_bound=config['upper_bound'])

    print('***** Results *****')
    print('Integral value found: ', res)
    print('*******************')
