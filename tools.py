import matplotlib.pyplot as pl

from mpl_toolkits.mplot3d import Axes3D


def singleton(class_):
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


def scatter_3d(x, y, z):
    f = pl.figure()
    ax = f.add_subplot(111, projection='3d')

    ax.scatter(x, y, z)
    pl.show()
