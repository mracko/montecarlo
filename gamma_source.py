#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as pl

from mpl_toolkits.mplot3d import Axes3D

# ========= Config =========

config = {
    'n_emissions': 1,
    'n_gammas': 1000000,
    'emitter_size': 1
}

# ==========================


class SphericalEmitter(object):
    def __init__(self,
                 size):
        self.size = size

        self.n_absorbed = 0
        self.n_emitted = 0
 
    def _fill_random_sphere(self,
                            sphere_radius,
                            n_points: int) -> np.array:
        """
        Returns x, y, z coords of points randomly
        distributed in a sphere
        """
        radii = np.random.random(size=n_points) * sphere_radius
        radii = np.cbrt(radii)

        azi = np.random.random(size=n_points) * 2 * np.pi

        pol = np.random.random(size=n_points) * 2
        pol = np.arccos(1 - pol)

        x = radii * np.sin(pol) * np.cos(azi)
        y = radii * np.sin(pol) * np.sin(azi)
        z = radii * np.cos(pol)

        return np.array([x, y, z])

    def shine(self,
              n_gammas: int):
        positions = self._fill_random_sphere(self.size, n_gammas)

        f = pl.figure()
        ax = f.add_subplot(111, projection='3d')

        print(positions[0])
        print(positions[1])
        print(positions[2])

        ax.scatter(positions[0], positions[1], positions[2])
        pl.show()

        distances = np.random.exponential(size=n_gammas)

        interaction_vectors = self._fill_random_sphere(distances, n_gammas)

        positions += interaction_vectors

        interaction_distances = np.sqrt(sum(positions ** 2))

        self.n_absorbed += sum(interaction_distances <= self.size)
        self.n_emitted += n_gammas


if __name__ == '__main__':
    n_emissions = config['n_emissions']
    n_gammas = config['n_gammas']
    emitter_size = config['emitter_size']

    emittor = SphericalEmitter(emitter_size)

    for i in range(n_emissions):
        emittor.shine(n_gammas)

    print('***** Results *****')
    print('Absorbed / emitted: ', emittor.n_absorbed / emittor.n_emitted)
    print('*******************')
