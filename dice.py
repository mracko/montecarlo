#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as pl

from math import ceil
from tools import singleton


# ========= Config =========

config = {
    'n_dice': 2,
    'n_sides': 6,
    'n_rolls': 10000000,
    'value_of_interest': 12
}

# ==========================


class Die(object):
    def __init__(self,
                 n_sides=6):
        self.value = None

        self.n_sides = n_sides

    def roll(self):
        r = np.random.random()

        self.value = ceil(r * self.n_sides)

        return self.value


@singleton
class DieRoller(object):
    def __init__(self,
                 n_dice: int,
                 die_base=Die,
                 n_sides=6):
        self.dice = [die_base(n_sides) for i in range(n_dice)]

        self.values = None

    def roll_all(self,
                 n_rols: int):
        self.values = np.zeros(n_rols)

        for i in range(n_rols):
            current_score = 0

            for die in self.dice:
                current_score += die.roll()
            
            self.values[i] = current_score

        return self.values


if __name__ == '__main__':
    die_roller = DieRoller(n_dice=config['n_dice'],
                           n_sides=config['n_sides'])

    values = die_roller.roll_all(n_rols=config['n_rolls'])

    n_wins = sum(values == config['value_of_interest'])

    print('***** Results *****')
    print('N positive results: ', n_wins)
    print('Total fraction: ', n_wins / config['n_rolls'])
    print('*******************')

    min_value = config['n_dice']
    max_value = config['n_dice'] * config['n_sides']

    if max_value - min_value < 10:
        n_bins = max_value - min_value

    else:
        n_bins = None

    pl.hist(values, rwidth=1, bins=n_bins, color='g', alpha=0.8)
    pl.title('Histogram of dice rolls', fontsize=20)
    pl.show()
