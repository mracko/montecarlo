#!/usr/bin/env python3

import numpy as np


# ========= Config =========

config = {
    'n_trials': 100000
}

# ==========================


class UnitCircle(object):
    def __init__(self):
        self.x_values = None
        self.y_values = None

    @staticmethod
    def _resample(values: np.ndarray,
                  min_val: float,
                  max_val: float) -> np.ndarray:
        values *= (max_val - min_val)
        values += min_val

        return values

    def random_vectors(self,
                       n_vectors: int):
        r = np.random.rand(n_vectors, 2)

        x = self._resample(r.T[0], -1, 1)
        y = self._resample(r.T[1], -1, 1)

        n = np.sqrt(x**2 + y**2)

        self.x_values = x / n
        self.y_values = y / n


if __name__ == '__main__':
    n_trials = config['n_trials']

    unit_circle = UnitCircle()

    unit_circle.random_vectors(n_trials)

    x = unit_circle.x_values
    y = unit_circle.y_values

    print(np.sqrt(x**2 + y**2))

    accepted = y[x > 0]

    print('***** Results *****')
    print('Sine wave integral: ', sum(abs(accepted)) / len(accepted))
    print('*******************')
