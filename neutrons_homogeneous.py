#!/usr/bin/env python3
import copy

import numpy as np
import matplotlib.pyplot as pl

from typing import List


# ========= Config =========

config = {
    'n_neutrons': 100,
    'max_distance': 10
}

# ==========================


class Neutron(object):
    def __init__(self,
                 max_dist: float):
        self.position = np.zeros(3)

        self.positions = []

        self.max_dist = max_dist

    def _next_collision(self):
        direction = np.random.random(3)

        direction *= 2

        direction -= np.ones(3)

        direction /= np.sqrt(sum(direction ** 2))

        distance = np.random.exponential()

        self.position += direction * distance

    def propagate(self) -> List[float]:
        while True:
            self._next_collision()

            dist = np.sqrt(sum(self.position ** 2))

            if dist < self.max_dist:
                self.positions.append(copy.deepcopy(self.position))
            else:
                break

        return self.positions


if __name__ == '__main__':
    n_neutrons = config['n_neutrons']
    max_dist = config['max_distance']

    collision_points = []

    for i in range(n_neutrons):
        neutron = Neutron(max_dist)

        collision_points += neutron.propagate()

    collision_points = np.array(collision_points).T

    collision_distances = np.sqrt(sum(collision_points ** 2))

    col_histo = np.histogram2d(collision_points[0],
                               collision_points[1],
                               bins=[40, 40])

    pl.hist(collision_distances, bins=50, color='g', alpha=0.8)
    pl.ylabel('Collisions', fontsize=16)
    pl.xlabel('Radius [mfp]', fontsize=16)
    pl.show()

    pl.imshow(col_histo[0], cmap='seismic')
    pl.show()
