#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as pl

from tools import singleton


# ========= Config =========

config = {
    'n_children': 10000,
    'n_steps': 10,
    'falling_probability': 0.1
}

# ==========================


class Child(object):
    def __init__(self,
                 falling_probability=0.1):
        self.standing = True

        self.n_steps_made = 0

        self.falling_probability = falling_probability

    def _step(self):
        draw = np.random.random()

        if draw < self.falling_probability:
            self.standing = False

    def walk(self,
             n_steps: int):
        for i in range(n_steps):
            self._step()

            if not self.standing:
                break

            self.n_steps_made += 1


@singleton
class ChildFactory(object):
    def __init__(self,
                 child_base=Child):
        if not hasattr(child_base, 'walk'):
            raise Exception('Method "walk" must be implemented in child_base')

        self.child_base = child_base

        self.children = None

    def get_children(self,
                     n_children: int,
                     falling_probability=0.1):
        self.children = []

        for i in range(n_children):
            child = self.child_base(falling_probability)

            self.children.append(child)

        return self.children


if __name__ == '__main__':
    factory = ChildFactory()

    children = factory.get_children(
        n_children=config['n_children'],
        falling_probability=config['falling_probability'])

    n_steps = config['n_steps']

    for child in children:
        child.walk(n_steps)

    steps_made = np.array([child.n_steps_made for child in children])
    successfull = np.array([child.standing for child in children])

    print('***** Results *****')
    print('Number of successfull atempts: ', sum(successfull), '/', len(successfull))
    print('*******************')

    if n_steps < 10:
        n_bins = n_steps

    else:
        n_bins = None

    pl.hist(steps_made, rwidth=1, bins=n_bins, color='b', alpha=0.8)
    pl.title('Numbers of steps done')
    pl.show()
